﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelApp = Microsoft.Office.Interop.Excel;

namespace NonprimeNumberPyramid
{
    public partial class Form1 : Form
    {
        Params parameters = new Params();
        public Form1()
        {
            InitializeComponent();
            
            //DosyaYolu = "C:/Users/SumeyraIcen/Source/Repos/NonprimeNumberPyramid/ExcelFile/Q-25-Values.xlsx";
            //fileTextBox.Text = DosyaYolu;
            //FindMaxNumber();
        }

        public void FindMaxNumber()
        {

            ExcelApp.Application excelApp = new ExcelApp.Application();  
            if (excelApp == null)
            {
                MessageBox.Show("Excel yüklü değil.");    
                return;
            }
            ExcelApp.Workbook excelBook = excelApp.Workbooks.Open(parameters.DosyaYolu);
            ExcelApp._Worksheet excelSheet = excelBook.Sheets[1];
            ExcelApp.Range excelRange = excelSheet.UsedRange;   
            int satirSayisi = excelRange.Rows.Count; 
            int sutunSayisi = excelRange.Columns.Count;
            parameters.maxArray = ToDataTable(excelRange,satirSayisi,sutunSayisi);
            int result = FindMax(parameters.maxArray);
            ResultTextBox.Text = result.ToString();
            resultLabel.Visible = true;
            ResultTextBox.Visible = true;

            excelApp.Quit();
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
        }

        public List<int> ToDataTable(ExcelApp.Range range, int rows, int cols)
        {
            var value = "";
            int max = 0;
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= cols; j++)
                {
                    if (range.Cells[i, j] != null && range.Cells[i, j].Value2 != null)
                    {
                        value = range.Cells[i, j].Value2.ToString();
                        int deger = Convert.ToInt32(value);
                        bool prime = AsalSayi(deger);
                        if (prime)
                        {
                            parameters.maxArray = CheckNearorCross(deger,j,i);
                        }
                    }
                }

            }
            return parameters.maxArray;
        }
        private static bool AsalSayi(int sayi)
        {    
            bool result = true;   
            for (int i = 2; i<sayi - 1; i++)   
            {        
                if (sayi % i == 0)        
                {            
                    result = false;            
                    i = sayi;        
                }   
            }    
            if (sayi < 2) result = false;
            return result;
        }
        private int FindMax(List<int> numbers)
        {
            int number1 = 0;
            for (int i=0; i<numbers.Count; i++)
            {
                if(numbers[i]>number1)
                number1 = numbers[i];
            }
            return number1;
        }

        private List<int> CheckNearorCross(int value, int column, int row)
        {
            int toplam = 0;
            if(((column == parameters.oldColumn + 1) && (row == parameters.oldRow + 1)) || ((row == parameters.oldRow) && (column == parameters.oldColumn + 1)))
            {
                parameters.add = true;
                bool contain = parameters.maxColumn.Contains(parameters.oldValue);
                if(!contain)
                    parameters.maxColumn.Add(parameters.oldValue);
                parameters.maxColumn.Add(value);
                parameters.c = column;
                parameters.r = row;
                parameters.oldColumn = column;
                parameters.oldRow = row;
            }
            else
            {
                if (parameters.add)
                {
                    for (int columnNo = 0; columnNo < parameters.maxColumn.Count(); columnNo++)
                    {
                        toplam += parameters.maxColumn[columnNo];
                    }
                }
                if(toplam!=0)
                    parameters.maxArray.Add(toplam);

                parameters.oldValue = 0;
                if ((parameters.c == 0 && parameters.r == 0) || ((row > parameters.r + 1) && (column > parameters.c + 1)))
                {
                    parameters.oldValue = value;
                    parameters.add = false;
                    parameters.oldColumn = column;
                    parameters.oldRow = row;
                    parameters.maxColumn = new List<int>();
                }
            }
            return parameters.maxArray;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void resultButton_Click(object sender, EventArgs e)
        {
            if (fileTextBox.Text == "")
                MessageBox.Show("Lütfen gerekli excel dosyasının yolunu (../../Q-25-Values.xlsx) formatı olacak şekilde giriniz.");
            else
            {
                parameters.DosyaYolu = fileTextBox.Text;
                FindMaxNumber();
            }
        }
    }
}
