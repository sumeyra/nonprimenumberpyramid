﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonprimeNumberPyramid
{
    public class Params
    {
        public List<int> maxArray = new List<int>();
        public List<int> maxColumn = new List<int>();
        public int oldColumn = 0;
        public int oldRow = 0;
        public int oldValue = 0;
        public bool add = false;
        public int r = 0;
        public int c = 0;
        public string DosyaYolu;
    }
}
